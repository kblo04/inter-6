﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombo : MonoBehaviour
{
    private Animator animator;
    [HideInInspector]public bool comboIsPlaying;
    public Combo[] combos;
    private int comboHits = 0;
    private bool comboisFinished;
    private PlayerManager playerManager;

    void Start()
    {
        animator = GetComponent<Animator>();
        playerManager = GetComponent<PlayerManager>();
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < combos.Length; i++)
        {
            if (/*Input.inputString == combos[i].inputCombo ||*/ Input.GetButtonDown(combos[i].inputCombo) && comboHits == 0 && !comboIsPlaying && !comboisFinished && !playerManager.isZoomed)
            {
                StartCombo(combos[i]);
            }

            if(comboIsPlaying && Input.GetButtonDown(combos[i].inputCombo) && comboHits < combos[i].hits.Length-1)
            {
                comboHits++;
                ComboSequence(combos[i], comboHits);
            }

            if (comboHits == combos[i].hits.Length )
            {
                comboisFinished = true;
                ResetCombo();
                Debug.Log("entrou aqui");
            }

        }
    }

    void StartCombo(Combo _combo)
    {
        animator.SetTrigger(_combo.hits[0].animationParameter);
    }

    void ComboSequence(Combo _combo ,int hitSequence)
    {
        animator.SetTrigger(_combo.hits[hitSequence].animationParameter);
    }

    private void ResetCombo()
    {
        comboHits = 0;
        comboIsPlaying = false;
        comboisFinished = false;
    }


    public void AnimationComboIsPlaying()
    {
        comboIsPlaying = true;
    }
}


