﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "New Combo", menuName = "Combo")]
public class Combo : ScriptableObject
{
    public Hit[] hits;
    public string inputCombo;



}
[Serializable]
public class Hit
{
    public string animationParameter;

}
