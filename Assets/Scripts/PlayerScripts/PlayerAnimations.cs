﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    // Start is called before the first frame update
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void IdleAnimation()
    {
        animator.SetFloat("Speed", 0f);
        animator.SetBool("Movement", true);
    }

    public void RunningAnimation()
    {
        animator.SetBool("Movement", true);
        animator.SetFloat("Speed", 1f);

    }

    public void BloodAttackAnimation()
    {
        animator.SetFloat("Speed", 0f);
        animator.SetTrigger("BloodAttack");
        animator.SetBool("Movement", false);
    }

    public void TransformAnimation()
    {
        animator.SetFloat("Speed", 0f);
        animator.SetTrigger("Transform");
        animator.SetBool("Movement", false);
    }
}
