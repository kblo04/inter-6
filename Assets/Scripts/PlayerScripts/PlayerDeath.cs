﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour
{
    private PlayerManager playerManager;
    private Animator animator;
    private HitPoints hitPoints;
    private AudioSource audioSource;
    public GameObject restartScreen;
    public GameObject deathVfx;
    public SoundBank soundBank;
    void Start()
    {
        playerManager = GetComponent<PlayerManager>();
        animator = GetComponent<Animator>();
        hitPoints = GetComponent<HitPoints>();
        audioSource = GetComponent<AudioSource>();
        restartScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (hitPoints.currentPoints == 0)
            Die();
    }

    public void Die()
    {
        playerManager.isZoomed = false;
        playerManager.isDead = true;
        animator.SetTrigger("Dead");
    }

    public void ActiveRestartScreen()
    {
        restartScreen.SetActive(true);
        GameState.gamePaused = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void DesactiveRestartScreen()
    {
        restartScreen.SetActive(false);        
    }

    public void DeathEffects()
    {
        Instantiate(deathVfx, transform.position, transform.rotation);
        audioSource.clip = soundBank.death.audioClip[Random.Range(0, soundBank.death.audioClip.Length)];
        audioSource.pitch = 1;
        audioSource.Play();
    }


}
