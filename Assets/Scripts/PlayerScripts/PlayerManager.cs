﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using Cinemachine.PostFX;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{

    private PlayerAnimations playerAnimations;
    private PlayerController playerController;
    private PlayerCombo playerCombo;
    private Animator animator;
    private AudioSource audioSource;
    private BatMode batMode;

    [HideInInspector] public HitPoints hitPoints;
    [HideInInspector] public BloodPoints bloodPoints;
    private MeleeAttack meleeAttack;
    [HideInInspector] public FreezeAnimation freezeAnimation;


    public MeleeHand meleeHand;
    public MeleeHand bloodHand;

    public SoundBank soundBank;
    public int baseMeleeDamage = 15;
    [HideInInspector] public int meleeDamage;
    public float freezeTime = .25f;
    public int bloodAttackDamage = 20;
    public int bloodAttackCharge = 5;
    public float bloodAttackCD = 5;
    [SerializeField] private Image bloodAttackCD_UI;
    public float nextTimetoBloodAttack;
    [HideInInspector] public string target = "Enemy";

    [HideInInspector] public bool isAttacking = false;


    public float baseLifeRegenRate = 10;
    public float lifeRegenRate;
    public float baseLifeRegenTime = 3;
    private float lifeRegenTime = 0;

    public float dashSpeed = 200f;
    public float dashCD = 5f;
    private float nextTimeToDash;
    private bool isWalking;
    [HideInInspector] public bool isStunned;

    public float zoom = 4f;
    public float xZoomValue;
    private float normal = 10.41f;
    private float xNormalValue = 0.5f;
    private float smooth = 5;
    [HideInInspector] public bool isZoomed;
    public CinemachineFreeLook fCam;
    [HideInInspector] public CinemachineVolumeSettings volumeSettings;
    public GameObject aim;
    private RaycastShooting raycastShooting;
    public float shootRange = 50;
    public int shootDamage = 7;
    public int shootBloodCost = 2;
    public float shootRadius = .5f;

    [HideInInspector] public bool isDead;
    public GameObject spawnVfx;
    public Highlight highlight;
    [HideInInspector] public bool isWaitingCutscene;

    public Slider cameraVel;
    public Slider aimingCameraVel;

    public float meleeCheckRange = 1f;
    public Vector3 offsetRayAngle = new Vector3(0.1f, 0, 0);
    private Vector3 upOffset = new Vector3(0, 1f, 0);
    public int rayNum = 3;

    void Start()
    {
        if(GameState.playerSpawn != new Vector3(0,0,0))
        {
            transform.position = GameState.playerSpawn;
            Debug.Log("RESPAWN ON CHECKPOINT");
        }


        playerAnimations = GetComponent<PlayerAnimations>();
        playerController = GetComponent<PlayerController>();
        playerCombo = GetComponent<PlayerCombo>();
        animator = GetComponent<Animator>();
        hitPoints = GetComponent<HitPoints>();
        meleeAttack = GetComponent<MeleeAttack>();
        audioSource = GetComponent<AudioSource>();
        hitPoints.onDamage += TakeDamageEffects;
        bloodPoints = GetComponent<BloodPoints>();
        raycastShooting = GetComponent<RaycastShooting>();
        freezeAnimation = GetComponent<FreezeAnimation>();
        batMode = GetComponent<BatMode>();
        volumeSettings = fCam.GetComponent<CinemachineVolumeSettings>();

        playerController.onDashEnter += ImortalOn;
        playerController.onDashExit += ImortalOff;
        hitPoints.onDamage += Freeze;
        Instantiate(spawnVfx, new Vector3(transform.position.x, transform.position.y - 0.55f, transform.position.z), transform.rotation);
        aim.SetActive(false);

        meleeDamage = baseMeleeDamage;
    }

    //private void OnEnable()
   // {
        //hitPoints.onHitTaken += TakeDamageEffects;
        //meleeAttack.onAttack += AttackEffects;
        
 //   }

    private void OnDisable()
    {
        playerController.onDashEnter -= ImortalOn;
        playerController.onDashExit -= ImortalOff;
        hitPoints.onDamage -= TakeDamageEffects;
        //meleeAttack.onAttack -= AttackEffects;
    }


    void Update()
    {
        CheckBloodAttackCD();

        CheckMovement();

        CheckDash();

        CheckZoom();

        CheckCheats();

        CheckAttack();

        RegenHP();

        Debug.Log("Invulnerable: " + hitPoints.invulnerable);


        if (isAttacking)
            CheckEnemiesNearby(); // last method
    }

    private void CheckEnemiesNearby()
    {
        RaycastHit hit1;
        RaycastHit[] raysLeft = new RaycastHit[rayNum];
        RaycastHit[] raysRight = new RaycastHit[rayNum];


        if (Physics.Raycast(transform.position + upOffset, transform.TransformDirection(Vector3.forward), out hit1, meleeCheckRange))
        {
            Debug.DrawRay(transform.position + upOffset, transform.TransformDirection(Vector3.forward) * meleeCheckRange, Color.blue);
            if (hit1.transform.GetComponent<StateController>())
            {
                RotateTo(hit1.transform);
                return;
            }
        }


        for (int i = 0; i < raysLeft.Length; i++)
        {
            if (Physics.Raycast(transform.position + upOffset, transform.TransformDirection(Vector3.forward + -offsetRayAngle * (i + 1)), out raysLeft[i], meleeCheckRange))
            {
                Debug.DrawRay(transform.position + upOffset, transform.TransformDirection(Vector3.forward + -offsetRayAngle * (i + 1)) * meleeCheckRange, Color.blue);
                if (raysLeft[i].transform.GetComponent<StateController>())
                {
                    RotateTo(raysLeft[i].transform);
                    return;
                }
            }
        }

        for (int i = 0; i < raysRight.Length; i++)
        {
            if (Physics.Raycast(transform.position + upOffset, transform.TransformDirection(Vector3.forward + offsetRayAngle * (i + 1)), out raysLeft[i], meleeCheckRange))
            {
                Debug.DrawRay(transform.position + upOffset, transform.TransformDirection(Vector3.forward + offsetRayAngle * (i + 1)) * meleeCheckRange, Color.blue);
                if (raysLeft[i].transform.GetComponent<StateController>())
                {
                    RotateTo(raysLeft[i].transform);
                    return;

                }
            }
        }

    }

    private void RotateTo(Transform _enemyTransfor)
    {
        Vector3 direction = (_enemyTransfor.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10f);
    }

    private void CheckAttack()
    {
        if (!CheckIfAnimationFinished("Attack1") || !CheckIfAnimationFinished("Attack2") || !CheckIfAnimationFinished("Attack3") || !CheckIfAnimationFinished("BloodAttack"))
        {
            isAttacking = true;
        }
        else
        {
            isAttacking = false;
        }

        if (!batMode.isInBatMode)
        {
            bloodPoints.CheckDrain();
        }
    }

    private void CheckCheats()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            bloodPoints.Heal(100);
        }

        if (Input.GetKeyDown(KeyCode.F2))
        {
            hitPoints.invulnerable = !hitPoints.invulnerable;
        }

        if(Input.GetKeyDown(KeyCode.F3))
        {
            hitPoints.Damage(50);
        }

        if(Input.GetKeyDown(KeyCode.F4))
        {
            if(meleeDamage <= baseMeleeDamage)
            {
                meleeDamage = 9999;
            }
            else if(meleeDamage > baseMeleeDamage)
            {
                meleeDamage = baseMeleeDamage;
            }
            
        }
    }

    private void CheckDash()
    {
        nextTimeToDash -= Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.Space) && nextTimeToDash <= 0)
        {
            StartDash();
            nextTimeToDash = dashCD;
        }
    }

    private void CheckBloodAttackCD()
    {
        nextTimetoBloodAttack += Time.deltaTime;
        bloodAttackCD_UI.fillAmount = nextTimetoBloodAttack / bloodAttackCD;

        if (Input.GetKeyDown(KeyCode.Q) && nextTimetoBloodAttack >= bloodAttackCD)
        {
            playerAnimations.BloodAttackAnimation();
            nextTimetoBloodAttack = 0;
        }
    }

    private void CheckMovement()
    {
        if (Input.GetAxisRaw("Vertical") == 0f && Input.GetAxisRaw("Horizontal") == 0f)
        {
            isWalking = false;
        }
        else if (Input.GetAxis("Vertical") != 0f || Input.GetAxis("Horizontal") != 0f)
        {
            isWalking = true;
        }

        if (isAttacking || isStunned || isDead || isWaitingCutscene)
        {
            isWalking = false;
        }

        if (isWalking)
        {
            Walking();
        }

        if (!isWalking)
            Idle();
    }

    private void CheckZoom()
    {

        if (Input.GetMouseButtonDown(1) && !batMode.isInBatMode)
        {
            isZoomed = !isZoomed;
        }

        if (isZoomed)
        {
            ZoomIn();
            aim.GetComponent<Image>().color = Color.blue;
            if (Input.GetMouseButtonDown(0) && bloodPoints.currentPoints >= shootBloodCost)
            {
                Shoot();
            }
            else if (Input.GetMouseButtonDown(0) && bloodPoints.currentPoints < shootBloodCost)
            {
                aim.GetComponent<Image>().color = Color.red;
            }

        }
        else
            ZoomOut();
    }

    private void Shoot()
    {
        if (raycastShooting.ShootBullet(shootRange, shootRadius) != null)
        {
            bloodPoints.Damage(shootBloodCost);
            raycastShooting.ShootBullet(shootRange, shootRadius);
            animator.SetTrigger("Shoot");
            audioSource.clip = (soundBank.rangedAttack.audioClip[Random.Range(0, soundBank.rangedAttack.audioClip.Length - 1)]);
            audioSource.pitch = 1;
            audioSource.volume = soundBank.rangedAttack.volume;
            audioSource.Play();
            if (raycastShooting.ShootBullet(shootRange, shootRadius).GetComponent<HitPoints>())
            {
                raycastShooting.ShootBullet(shootRange, shootRadius).GetComponent<HitPoints>().Damage(shootDamage);
            }
        }
        else
            aim.GetComponent<Image>().color = Color.red;

    }

    public void MeleeAttackOn()    //Call in animation event to activate MELEE attack
    {
       // audioSource.clip = (soundBank.meleeAttack.audioClip[Random.Range(0, soundBank.meleeAttack.audioClip.Length)]);
       // audioSource.volume = soundBank.meleeAttack.volume;
        audioSource.pitch = 1;
        audioSource.PlayOneShot(soundBank.meleeAttack.audioClip[Random.Range(0, soundBank.meleeAttack.audioClip.Length)], soundBank.meleeAttack.volume);
        meleeHand.onDamageDealt += Freeze;
        meleeAttack.AttackOn(meleeDamage, target, meleeHand);
    }

    public void MeleeAttackOff()    //Call in animation event to deactivate MELEE attack
    {
        isAttacking = false;
        meleeHand.onDamageDealt -= Freeze;
        freezeAnimation.ForceUnfreeze();
        meleeAttack.AttackOff(meleeHand);
    }

    public void BloodAttackOn()    //Call in animation event to activate BLOOD attack
    {
        isAttacking = true;
        hitPoints.invulnerable = true;
        audioSource.clip = (soundBank.bloodAttack.audioClip[Random.Range(0, soundBank.bloodAttack.audioClip.Length)]);
        audioSource.pitch = 1;
        audioSource.volume = soundBank.bloodAttack.volume;
        audioSource.Play();

        MeleeAttackOff();
        bloodHand.onDamageDealt += Freeze;
        meleeAttack.AttackOn(bloodAttackDamage, target, bloodHand);

    }

    public void BloodAttackOff()    //Call in animation event to deactivate BLOOD attack
    {
        isAttacking = false;
        hitPoints.invulnerable = false;
        bloodPoints.Heal(bloodAttackCharge * bloodHand.enemiesDamaged);
        bloodHand.onDamageDealt -= Freeze;
        freezeAnimation.ForceUnfreeze();
        meleeAttack.AttackOff(bloodHand);

    }

    private void Freeze()
    {
        freezeAnimation.Freeze(animator, freezeTime);
    }

    public void BloodAttackOnHitActions() //Add VFX and other stuff
    {
        bloodPoints.Heal(bloodAttackCharge);
    }

    public void TakeDamageEffects()
    {
        isZoomed = false;
        animator.SetTrigger("Stunned");
        isStunned = true;
        audioSource.clip = (soundBank.takeHit.audioClip[Random.Range(0, soundBank.takeHit.audioClip.Length)]);
        audioSource.pitch = 1;
        audioSource.volume = soundBank.takeHit.volume;
        audioSource.Play();
    }

    //public void AttackEffects()
    //{
    //    if (!audioSource.isPlaying)
    //    {
    //        audioSource.loop = false;
    //        audioSource.PlayOneShot(soundBank.meleeAttack.audioClip[Random.Range(0, soundBank.meleeAttack.audioClip.Length)], soundBank.meleeAttack.volume);

    //    }
    //}

    public void Idle()
    {
        playerAnimations.IdleAnimation();
    }

    public void Walking()
    {
        playerController.Move(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        playerAnimations.RunningAnimation();

    }

    public void PlayStepSfx()
    {
        audioSource.PlayOneShot(soundBank.step.audioClip[Random.Range(0, soundBank.step.audioClip.Length)], soundBank.step.volume);
        audioSource.pitch = Random.Range(soundBank.stepPitchMin, soundBank.stepPitchMax);

    }


    public void ExitStun()
    {
        isStunned = false;
    }

    private void StartDash()
    {
        animator.SetTrigger("Dash");
    }


    public void Dash()
    {
        audioSource.PlayOneShot(soundBank.dash.audioClip[Random.Range(0, soundBank.dash.audioClip.Length)], soundBank.dash.volume);
        playerController.Dash();
    }

    public void ImortalOn()
    {
        hitPoints.invulnerable = true;
    }
    public void ImortalOff()
    {
        hitPoints.invulnerable = false;
    }

    private bool CheckIfAnimationFinished(string animName)
    {
        return !animator.GetCurrentAnimatorStateInfo(0).IsName(animName);
    }

    private void RegenHP()
    {
        lifeRegenTime += Time.deltaTime;
        if (lifeRegenTime >= baseLifeRegenTime)
        {
            lifeRegenRate = baseLifeRegenRate * ((float)bloodPoints.currentPoints / bloodPoints.basePoints);
            hitPoints.Heal((int)lifeRegenRate);
            lifeRegenTime = 0;

            //Debug.Log("Regen Rate (int): " + (int)lifeRegenRate);
            //Debug.Log("Regen Rate (float): " + lifeRegenRate);
        }
    }

    private void ZoomIn()
    {
        var cameraRotation = Camera.main.transform.rotation;
        cameraRotation.x = 0;
        cameraRotation.z = 0;
        transform.rotation = Quaternion.Slerp(transform.rotation, cameraRotation, Time.deltaTime * smooth * 2);
        animator.SetBool("Aiming", true);
        aim.SetActive(true);
        fCam.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_ScreenX = Mathf.Lerp(fCam.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_ScreenX, xZoomValue, Time.deltaTime * smooth);
        fCam.m_Orbits[1].m_Radius = Mathf.Lerp(fCam.m_Orbits[1].m_Radius, zoom, Time.deltaTime * smooth);
        fCam.m_XAxis.m_MaxSpeed = aimingCameraVel.value * 50;
        fCam.m_YAxis.m_MaxSpeed = aimingCameraVel.value / 10;
        VolumeSettingsOn();
    }

    private void ZoomOut()
    {
        animator.SetBool("Aiming", false);
        aim.SetActive(false);
        fCam.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_ScreenX = Mathf.Lerp(fCam.GetRig(1).GetCinemachineComponent<CinemachineComposer>().m_ScreenX, xNormalValue, Time.deltaTime * smooth);
        fCam.m_Orbits[1].m_Radius = Mathf.Lerp(fCam.m_Orbits[1].m_Radius, normal, Time.deltaTime * smooth);
        fCam.m_XAxis.m_MaxSpeed = cameraVel.value * 100;
        fCam.m_YAxis.m_MaxSpeed = cameraVel.value / 4;
        if (!playerController.isDashing)
        {
            VolumeSettingsOff();
        }

    }

    public void VolumeSettingsOn()
    {
        volumeSettings.enabled = true;
    }

    public void VolumeSettingsOff()
    {
        volumeSettings.enabled = false;


    }
    public void WaitingCutscene(float _timeToWait)
    {
        isWaitingCutscene = true;
        StartCoroutine("StopWaiting", _timeToWait);
    }

    IEnumerator StopWaiting(float _timeToWait)
    {
        yield return new WaitForSeconds(_timeToWait);
        isWaitingCutscene = false;
    }
}
