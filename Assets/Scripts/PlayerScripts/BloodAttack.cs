﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloodAttack : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject bloodAttackVFX;
    private HitPoints hitPoints;
    public int enemiesInArea;
    public Slider bloodBar;

    private void OnEnable()
    {
        GameObject vfxPrefab = Instantiate(bloodAttackVFX, transform.position, transform.rotation);
        Destroy(vfxPrefab, 5f);
        BloodAttackArea();
        bloodBar.value += (enemiesInArea * 0.1f);
        hitPoints = GetComponentInParent<HitPoints>();
        hitPoints.Heal(enemiesInArea);
        StartCoroutine(DesactiveObj());
    }


    public void BloodAttackArea()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 3f);
        int i = 0;
        while (i < hitColliders.Length)
        {
            if (hitColliders[i].gameObject.tag == "Enemy")
            {
                enemiesInArea++;
            }
            i++;
        }
    }

    IEnumerator DesactiveObj()
    {
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        enemiesInArea = 0;
    }
}
