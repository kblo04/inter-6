﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BatMode : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject wings;
    public bool isInBatMode;
    public int buffDamage;
    public float buffVelocity;
    private PlayerController playerController;
    private PlayerManager playerManager;
    private PlayerAnimations playerAnimations;
    private PlayVfxs playVfxs;

    [SerializeField] private int bloodDrainRate = 1;
    [SerializeField] private float bloodDrainTime = .2f;


    void Start()
    {
        playerManager = GetComponent<PlayerManager>();
        playerController = GetComponent<PlayerController>();
        playerAnimations = GetComponent<PlayerAnimations>();
        wings.SetActive(false);
        playVfxs = GetComponent<PlayVfxs>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerManager.bloodPoints.currentPoints == playerManager.bloodPoints.basePoints && Input.GetKeyDown(KeyCode.R) && !isInBatMode)
        {
            playerAnimations.TransformAnimation();
         
        }

        if(playerManager.bloodPoints.currentPoints <= 1 && isInBatMode)
        {
            ExitBatMode();
        }
    }

    public IEnumerator DrainBlood()
    {
        while (true)
        {
            playerManager.bloodPoints.Damage(bloodDrainRate);
            yield return new WaitForSeconds(bloodDrainTime);
        }
    }

    public void EnterBatMode()
    {
        isInBatMode = true;
        wings.SetActive(true);
        playerController.speed += buffVelocity;
        playerManager.meleeDamage += buffDamage;
        StartCoroutine(DrainBlood());
    }

    public void ExitBatMode()
    {
        playVfxs.StopVfx(2);
        StopAllCoroutines();
        wings.SetActive(false);
        isInBatMode = false;
        playerController.speed -= buffVelocity;
        playerManager.meleeDamage = playerManager.baseMeleeDamage;
    }
}

