﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSounds : MonoBehaviour
{
    public AudioClip attack1SFX, attack2FX;
    public AudioClip walkSFX;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void IdleSFX()
    {
        if (audioSource.clip == walkSFX && audioSource.isPlaying)
            audioSource.Stop();
    }

    public void WalkingSFX()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.clip = walkSFX;
            audioSource.Play();
            audioSource.loop = true;
        }
    }

    public void Attack1SFX()
    {
        audioSource.loop = false;
        audioSource.clip = attack1SFX;
        audioSource.Play();
    }

    public void Attack2SFX()
    {
        audioSource.loop = false;
        audioSource.clip = attack2FX;
        audioSource.Play();
    }
}
