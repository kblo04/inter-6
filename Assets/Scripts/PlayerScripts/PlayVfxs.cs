﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayVfxs : MonoBehaviour
{
    // Start is called before the first frame update
    public VfxsData[] vfxs;

    public void PlayVfx(int _index)
    {
        if(vfxs != null)
        {
            for (int i = 0; i < vfxs[_index].particleSystems.Length; i++)
            {
                vfxs[_index].particleSystems[i].Play();
                if (vfxs[_index].isLoop)
                {
                    var main = vfxs[_index].particleSystems[i].main;
                    main.loop = true;
                }
            }
        }

    }

    public void StopVfx(int _index)
    {
        if(vfxs != null)
        {
            for (int i = 0; i < vfxs[_index].particleSystems.Length; i++)
            {
                if(!vfxs[_index].isLoop)
                {
                    vfxs[_index].particleSystems[i].Stop();
                }
                else
                {
                    var main = vfxs[_index].particleSystems[i].main;
                    main.loop = false;
                }
                

            }
        }
       
    }

}




