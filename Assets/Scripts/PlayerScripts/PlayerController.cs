﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    private float currentSpeed;
    private float speedSmoothVelocity = 0.1f;
    private float speedSmoothTime = 0.1f;
    private float rotationSpeed = 0.5f;
    private float gravity = 3f;
    private Vector3 desiredMoveDirection;
    [HideInInspector]public bool isDashing;
    public float dashTime;
    public float dashSpeed;

    CharacterController characterController;
    PlayerManager playerManager;

    public delegate void OnDashEnter();
    public event OnDashEnter onDashEnter;

    public delegate void OnDashExit();
    public event OnDashExit onDashExit;

    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        playerManager = GetComponent<PlayerManager>();

    }

    public void Move(float _horizontal, float _vertical)
    {
        Vector3 movementInput = new Vector3(_horizontal, 0, _vertical);

        Vector3 forward = Camera.main.transform.forward;
        Vector3 right = Camera.main.transform.right;

        forward.y = 0;
        right.y = 0;

        desiredMoveDirection = (forward * movementInput.z + right * movementInput.x).normalized;

        Vector3 gravityVector = Vector3.zero;

        if (!characterController.isGrounded)
        {
            gravityVector.y -= gravity;
        }

        if (desiredMoveDirection != Vector3.zero && !playerManager.isZoomed)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed);
        }

        float targetSpeed = speed * movementInput.magnitude;
        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, speedSmoothTime);

        characterController.Move(desiredMoveDirection * currentSpeed * Time.deltaTime);
        characterController.Move(gravityVector * Time.deltaTime);
    }

    private void Update()
    {
        if (isDashing)
        {
            StartCoroutine("StopDash", dashTime);
            playerManager.VolumeSettingsOn();
            characterController.Move(transform.forward * dashSpeed * Time.deltaTime);
        }
    }

    public void Dash()
    {
        isDashing = true;
        onDashEnter?.Invoke();
    }

    IEnumerator StopDash(float _dashTime)
    {
        yield return new WaitForSeconds(_dashTime);
        onDashExit?.Invoke();
        isDashing = false;
        playerManager.VolumeSettingsOff();

    }

}
