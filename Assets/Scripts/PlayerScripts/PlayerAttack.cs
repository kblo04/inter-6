﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    public Transform point0, point1;
    public float radius;
    private bool attacking;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.X))
        {
            attacking = true;
        }
        else
            attacking = false;

        if (attacking)
        {
            CheckAttack();
        }



    }

    void CheckAttack()
    {
        Collider[] hitColliders = Physics.OverlapCapsule(point0.position, point1.position, radius);
        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].GetComponent<HitPoints>())
            {
                hitColliders[i].GetComponent<HitPoints>().Damage(1);
            }
        }
    }
}
