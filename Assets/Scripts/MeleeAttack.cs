﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour
{
    public delegate void OnAttack();
    public event OnAttack onAttack;    


    public void AttackOn(int damage, string target, MeleeHand meleeHand)
    {
        meleeHand.damageToDo = damage;
        meleeHand.target = target;
        meleeHand.attackOn = true;
        meleeHand.hitbox.enabled = meleeHand.attackOn;
        onAttack?.Invoke();
    }


    public void AttackOff(MeleeHand meleeHand)
    {
        meleeHand.attackOn = false;
        meleeHand.hitbox.enabled = meleeHand.attackOn;
        meleeHand.enemiesDamaged = 0;
        meleeHand.damagedObjs.Clear();

    }
}
