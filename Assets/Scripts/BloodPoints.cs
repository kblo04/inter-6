﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodPoints : Points
{
    public float baseDrainCountDown = 7;
    private float drainCountDown = 0;

    public float baseDrainTime = 1;
    private float drainTime = 0;

    public int drainRate = 1;

    private void Awake()
    {
       onDamage += ResetDrainCountDown;
       onHeal += ResetDrainCountDown;
    }

    public override void Start()
    {
        currentPoints = 0;
        if (slider != null)
        {
            slider.maxValue = basePoints;
        }
    }

    public override void Update()
    {
        LimitPoints();
        UpdateSlider();        
    }

    public void CheckDrain()
    {
        drainCountDown += Time.deltaTime;

        if (drainCountDown >= baseDrainCountDown)
        {
            DrainBlood(baseDrainTime, drainRate);
        }
    }

    private void DrainBlood(float time, int rate)
    {
        drainTime += Time.deltaTime;
        if (drainTime >= baseDrainTime)
        {
            currentPoints -= rate;
            drainTime = 0;
        }        
    }

    public void ResetDrainCountDown()
    {
        drainCountDown = 0;
    }

}
