﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Highlight : MonoBehaviour
{

    public GameObject cameraGroup;
    public GameObject lastCameraGroup;
    private Animator animator;
    public PlayerManager playerManager;
    public TutorialManager tutorialManager;
    public BoxCollider box;
    public float freezeTime;
    public bool hasTutorial = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            cameraGroup.SetActive(true);
            playerManager.WaitingCutscene(freezeTime);
            box.enabled = false;
            if (lastCameraGroup != null)
                lastCameraGroup.SetActive(false);
            if (tutorialManager.gameObject.activeInHierarchy && hasTutorial)
                tutorialManager.ActivePanel(0);
        }

    }

}
