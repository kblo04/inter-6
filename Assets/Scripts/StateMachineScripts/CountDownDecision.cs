﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Decision_", menuName = "Troop/Decisions/CountDown")]
public class CountDownDecision : Decision
{

    public float timer;

    public override bool Decide(StateController controller)
    {
        bool isCountDownFinished = controller.CheckIfCountDownElapsed(timer);
        return isCountDownFinished;
    }
}
