﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Action_", menuName = "Troop/Actions/MeleeAttack")]
public class MeleeAttackAction : Action
{

    public override void Act(StateController controller)
    {
        Attack(controller);
    }

    private static void Attack(StateController controller)
    {
        controller.currentAnimation = "AttackMelee";
        controller.animator.SetTrigger("attack");
    }
}
