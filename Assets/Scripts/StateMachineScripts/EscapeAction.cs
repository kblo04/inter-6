﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Troop/Actions/Escape") ]
public class EscapeAction : Action
{
    public override void Act(StateController controller)
    {
        Escape(controller);
    }

    private void Escape(StateController controller)
    {
        controller.navMeshAgent.isStopped = false;

        Vector3 dirToTarget = controller.transform.position - controller.target.position;

        Vector3 newPos = controller.transform.position + dirToTarget;

        controller.navMeshAgent.SetDestination(newPos);
    }
}
