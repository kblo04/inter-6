﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateController : MonoBehaviour
{
    public State currentState;
    public State remainState;
    public State hitstunState;
    public State startingState;
    public State outOfCombatState;
    public State deadState;
    public MeleeHand[] meleehand = new MeleeHand[1];

    public TroopData troopData;
    public SoundBank soundBank;
    public Transform[] eyes = new Transform[1];
    public float freezeTime = 1f;

    [HideInInspector] public Rigidbody rb;
    [HideInInspector] public NavMeshAgent navMeshAgent;
    [HideInInspector] public MeleeAttack meleeAttack;
    [HideInInspector] public EnemyShooting enemyShooting;
    [HideInInspector] public Animator animator;
    [HideInInspector] public AudioSource audioSource;
    [HideInInspector] public HitPoints hitPoints;
    [HideInInspector] public TroopDeath troopDeath;
    [HideInInspector] public FreezeAnimation freezeAnimation;
    [HideInInspector] public bool allowHitstun = true;

    public List<Transform> wayPointList;
    [HideInInspector] public int nextWayPoint = 0;
    [HideInInspector] public Transform chaseTarget;
    [HideInInspector] public float stateTimeElapsed;
    [HideInInspector] public float thrustSpeed = 5;
    public Transform target;

    [HideInInspector] public string currentAnimation;
    [HideInInspector] public string currentAnimationTrigger;

    [HideInInspector] public bool endForce = false;


    private bool aiActive = true;

    void Awake()
    {
        enemyShooting = GetComponent<EnemyShooting>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.speed = troopData.movementSpeed;
        rb = GetComponent<Rigidbody>();
        meleeAttack = GetComponent<MeleeAttack>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        hitPoints = GetComponent<HitPoints>();
        hitPoints.basePoints = troopData.baseHP;
        troopDeath = GetComponent<TroopDeath>();
        currentState = outOfCombatState;
        freezeAnimation = GetComponent<FreezeAnimation>();
    }

    private void Start()
    {
        OnEnable();
    }

    private void OnEnable()
    {
        hitPoints.onDamage += Hitstun;
        hitPoints.onDamage += Freeze;
    }

    private void OnDisable()
    {
        hitPoints.onDamage -= Hitstun;
        hitPoints.onDamage -= Freeze;
    }

    private void Update()
    {
        troopDeath.CheckHP(hitPoints);

        if (!aiActive)
            return;
        currentState.UpdateState(this);
    }

    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            currentState = nextState;
            //Debug.Log("Enter State: " + nextState.name);
            OnExitState();
        }
    }

    private void OnExitState()
    {
        ResetCountDown();
    }

    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return stateTimeElapsed >= duration;
    }

    private void ResetCountDown()
    {
        stateTimeElapsed = 0;
    }

    public void StartForce()
    {
        endForce = false;
    }

    public void EndForce()
    {
        endForce = true;
    }

    public void Hitstun() //add to onHitTaken event in HitPoints script
    {
      // if (!audioSource.isPlaying)
      // {
           //audioSource.clip = (soundBank.takeHit.audioClip[Random.Range(0, soundBank.takeHit.audioClip.Length)]);
           //audioSource.volume = soundBank.takeHit.volume;
            audioSource.PlayOneShot(soundBank.takeHit.audioClip[Random.Range(0, soundBank.takeHit.audioClip.Length)], soundBank.takeHit.volume);
       // }
        if (allowHitstun)
        {
            currentState = hitstunState;
        }

    }

    public void MeleeAttackOn(int meleeHandIndex)    //Call on animation event
    {
        if (meleeAttack != null)
        {
            audioSource.clip = (soundBank.meleeAttack.audioClip[Random.Range(0, soundBank.meleeAttack.audioClip.Length)]);
            audioSource.volume = soundBank.meleeAttack.volume;
            audioSource.Play();
            meleeAttack.AttackOn(troopData.attackDamage, troopData.target, meleehand[meleeHandIndex]);
            meleehand[meleeHandIndex].onDamageDealt += Freeze;
        }
    }

    public void MeleeAttackOff(int meleeHandIndex)    //Call on animation Event
    {
        meleeAttack.AttackOff(meleehand[meleeHandIndex]);
        freezeAnimation.ForceUnfreeze();
        meleehand[meleeHandIndex].onDamageDealt -= Freeze;
    }

    public void AllMeleeAttackOff()
    {
        for (int i = 0; i < meleehand.Length; i++)
        {
            meleeAttack.AttackOff(meleehand[i]);
        }
    }

    public void StartCombat()
    {
        currentState = startingState;
        if (hitPoints.slider != null)
        {
            hitPoints.slider.gameObject.SetActive(true);
        }
    }

    void Freeze()
    {
        freezeAnimation.Freeze(animator, freezeTime);
    }

    public void ShootProjectiles()
    {
        enemyShooting.Fire(eyes, troopData.attackRate, troopData.attackDamage, troopData.attackForce);
    }

    public void Dead()
    {
        HpSliderOff();
        currentState = deadState;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = currentState.sceneGizmoColor;
        Gizmos.DrawWireSphere(transform.position, troopData.closeAreaRange);
        Gizmos.DrawWireSphere(transform.position, troopData.farAreaRange);
    }

    public void HpSliderOff()
    {
        if (hitPoints.slider)
            hitPoints.slider.gameObject.SetActive(false);
    }

    public void SpawnSpawners()
    {
        GetComponent<SpawnSpawners>().SpawnersOn();
    }
}