﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action_", menuName = "Troop/Actions/HitscanShoot")]
public class HitscanShootAction : Action
{
    public override void Act(StateController controller)
    {
        controller.navMeshAgent.isStopped = true;
    }
}
