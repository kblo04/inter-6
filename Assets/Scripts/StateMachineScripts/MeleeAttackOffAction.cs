﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action_", menuName = "Troop/Actions/MeleeAttackOff")]

public class MeleeAttackOffAction : Action
{
    public override void Act(StateController controller)
    {
        controller.AllMeleeAttackOff();
    }


}
