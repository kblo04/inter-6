﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TroopData_", menuName = "Troop/ TroopData")]
public class TroopData : ScriptableObject
{
    public int baseHP;
    public Vector3 meleeColliderSize;

    public int attackDamage;
    public float attackRadius;

    public float closeAreaRange = 3;
    public float farAreaRange;

    public string target;


    public float aimSpeed;
    public float attackRate;
    public float attackForce;


    public float movementSpeed;


}
