﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Action_", menuName = "Troop/Actions/SpawnSpawnersAction")]
public class SpawnSpawnersAction : Action
{

    public override void Act(StateController controller)
    {
        Spawn(controller);
    }

    private static void Spawn(StateController controller)
    {
        controller.navMeshAgent.isStopped = true;
        controller.SpawnSpawners();
    }
}