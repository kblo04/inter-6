﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField] GameObject pauseScreen;
    private bool done = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            GameState.gamePaused = !GameState.gamePaused;
            done = false;
        }

        if(GameState.gamePaused && !done)
        {
            PauseGame();
        }
        else if (!GameState.gamePaused && !done)
        {
            ResumeGame();
        }
    }

    public void PauseGame()
    {
        GameState.gamePaused = true;
        pauseScreen.SetActive(true);
        done = true;
    }

    public void ResumeGame()
    {
        GameState.gamePaused = false;
        pauseScreen.SetActive(false);
        done = true;
    }
}
