﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;

public class OptionsScreen : MonoBehaviour
{
    public Slider cameraVelSlider;
    public Text cameraVelSliderNum;
    public Slider soundVolSlider;
    public Text soundVolumeNum;
    public Slider aimingCamVelSlider;
    public Text aimingCamSliderNum;
    public Slider brighnessSlider;
    public Text brighnessSliderNum;
    public Volume volume;
    public LiftGammaGain liftGammaGain;
    public static float soundVol = 50;
    // Start is called before the first frame update
    void Start()
    {
        soundVolSlider.value = soundVol;
        AudioListener.volume = soundVolSlider.value / 100;
        LiftGammaGain tmp;
        if (volume)
        {
            if (volume.profile.TryGet<LiftGammaGain>(out tmp))
            {
                liftGammaGain = tmp;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        AudioListener.volume = soundVolSlider.value / 100;
        soundVol = AudioListener.volume * 100;
        cameraVelSliderNum.text = cameraVelSlider.value.ToString("F1");
        aimingCamSliderNum.text = aimingCamVelSlider.value.ToString("F1");
        soundVolumeNum.text = soundVolSlider.value.ToString();
        brighnessSliderNum.text = brighnessSlider.value.ToString("F1");
        if (liftGammaGain)
            liftGammaGain.gamma.value = new Vector4(brighnessSlider.value - 1, brighnessSlider.value - 1, brighnessSlider.value - 1, brighnessSlider.value - 1);


        if (Input.GetKeyDown(KeyCode.Escape) && gameObject.activeInHierarchy)
        {
            gameObject.SetActive(false);
        }
    }
}
