﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameState : MonoBehaviour
{

    public static bool gamePaused = false;
    [SerializeField] private float normalTimeScale = 1;
    public static Vector3 playerSpawn;

    void Update()
    {

        if(SceneManager.GetActiveScene().buildIndex == 0 || SceneManager.GetActiveScene().buildIndex == 1)
        {
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = normalTimeScale;
            Cursor.visible = true;
        }
        else if (gamePaused)
        {
            Time.timeScale = 0;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;

        }
        else if(!gamePaused)
        {
            Time.timeScale = normalTimeScale;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

        }

    }
}
