﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{

    [HideInInspector] public ProjectileShooting projectileShooting;
    public SoundBank soundBank;
    private AudioSource audioSource;
    private float nextFireTime = .5f;


    void Awake()
    {
        projectileShooting = GetComponent<ProjectileShooting>();
        audioSource = GetComponent<AudioSource>();
    }

    public void Fire(Transform[] shotPos, float fireRate, int damage, float attackForce)
    {
        if(Time.time > nextFireTime)
        {
            for (int i = 0; i < shotPos.Length; i++)
            {
                GameObject bulletInstance = projectileShooting.ShootBullet(shotPos[i].position, shotPos[i].rotation);
                bulletInstance.GetComponent<EnemyBullet>().damage = damage;
                bulletInstance.GetComponent<ProjectileBulletLauncher>().force = attackForce;
            }

            audioSource.clip = (soundBank.rangedAttack.audioClip[Random.Range(0, soundBank.rangedAttack.audioClip.Length)]);
            audioSource.volume = soundBank.rangedAttack.volume;
            audioSource.Play();

            nextFireTime = Time.time + fireRate;
        }
    }



}
