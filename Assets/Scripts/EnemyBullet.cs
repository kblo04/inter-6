﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{

    [HideInInspector]public int damage = 1;
    public Transform tip;
    public GameObject hitVFX;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Instantiate(hitVFX, tip.position, tip.rotation);

            BatMode hasBatmode = other.GetComponent<BatMode>();

            if (hasBatmode)
            {
                if (hasBatmode.isInBatMode)
                {
                    other.GetComponent<BloodPoints>().Damage(damage);
                }
                else
                    other.GetComponent<HitPoints>().Damage(damage);
            }
            else if (other.GetComponent<HitPoints>())
                other.GetComponent<HitPoints>().Damage(damage);
        }

        else if (other.gameObject.tag != "Enemy" && !other.gameObject.GetComponent<CheckLevel2Done>()  && !other.gameObject.GetComponent<EnemyArea>())
        {
            SelfDestruct();
        }

    }

    private void SelfDestruct()
    {
        ObjectPooler.SharedInstance.PoolDestroy(gameObject);
    }
}
