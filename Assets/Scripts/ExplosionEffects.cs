﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "ExplosionEffects_", menuName = "ExplosionEffects")]

public class ExplosionEffects : ScriptableObject
{
    public SoundData explosionSFXs;
    public SoundData hitSFXs;

}
