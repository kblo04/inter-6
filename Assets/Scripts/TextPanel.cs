﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextPanel : MonoBehaviour
{
    public static TextPanel sharedInstance;
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
        sharedInstance = this;
    }

    public void TextOn(string text)
    {
        GetComponentInChildren<Text>().text = text;
        animator.SetBool("textOn", true);
    }

    public void TextOff()
    {
        animator.SetBool("textOn", false);
    }
}
