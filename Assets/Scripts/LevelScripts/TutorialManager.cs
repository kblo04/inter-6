﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    // Start is called before the first frame update

    public static bool tutorialisOn;
    public GameObject[] panels;
    public Slider bloodBar;
    private bool sawPanel3;
    private bool sawPanel4;
 

    // Update is called once per frame
    void Update()
    {
        if (tutorialisOn)
        {
            if (panels[0].activeInHierarchy && Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
            {
                DesactivePanel(0);
            }

            if (panels[1].activeInHierarchy && Input.GetKeyDown(KeyCode.Q))
            {
                DesactivePanel(1);
            }

            if (bloodBar.value > 0 && !sawPanel3)
            {
                ActivePanel(2);

            }

            if (Input.GetMouseButton(1) && panels[2].activeInHierarchy)
            {
                DesactivePanel(2);
                sawPanel3 = true;
            }

            if (bloodBar.value == bloodBar.maxValue && !sawPanel4)
            {
                ActivePanel(3);

            }

            if (Input.GetKeyDown(KeyCode.R) && panels[3].activeInHierarchy)
            {
                DesactivePanel(3);
                sawPanel4 = true;
                gameObject.SetActive(false);
            }
        }
        else
             gameObject.transform.parent.gameObject.SetActive(false);
    }

    public void ActivePanel(int _panelNum)
    {
        panels[_panelNum].SetActive(true);
    }

    public void DesactivePanel(int _panelNum)
    {
        panels[_panelNum].SetActive(false);

    }


    public void TutorialOn()
    {
        tutorialisOn = true;
    }

    public void TutorialOff()
    {
        tutorialisOn = false;
    }
}
