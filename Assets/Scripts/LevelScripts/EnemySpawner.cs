﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public bool spawnerOn = false;

    public GameObject enemy;
    public int enemiesTotalNum;
    public int enemiesPerWave;
    public float timeToSpawn;
    public Transform player;
    private float nextTimetoSpawn;
    private int activeEnemies;
    public List<GameObject> enemies = new List<GameObject>();

    private EnemyArea enemyArea;

    private void Awake()
    {
        enemyArea = GetComponentInChildren<EnemyArea>();
        enemyArea.onCombatActivation += ActivateSpawner;
    }

    void Start()
    {
        InstantiateEnemies();
        FillList();
        nextTimetoSpawn = timeToSpawn;

    }

    private void OnDisable()
    {
        enemyArea.onCombatActivation -= ActivateSpawner;
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnerOn)
        {
            if (enemies.Count > 0)
            {
                timeToSpawn -= Time.deltaTime;
                if (timeToSpawn <= 0)
                {
                    ActiveEnemies();
                    timeToSpawn = nextTimetoSpawn;
                }
            }
        }

    }

    void ActivateSpawner()
    {
        spawnerOn = true;
    }

    void InstantiateEnemies()
    {
        for (int i = 0; i < enemiesTotalNum; i++)
        {
            GameObject enemyInst = Instantiate(enemy, transform.position, transform.rotation);
            enemyInst.transform.parent = gameObject.transform;
            enemyInst.GetComponent<StateController>().target = player;
            enemyInst.GetComponent<StateController>().StartCombat();
            enemyInst.SetActive(false);
        }
    }

    void FillList()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<StateController>())
                enemies.Add(transform.GetChild(i).gameObject);
        }
    }


    void ActiveEnemies()
    {
        if (enemies.Count > enemiesPerWave)
        {
            for (int i = 0; i < enemiesPerWave; i++)
            {
                enemies[i].SetActive(true);
                enemies.RemoveAt(i);
                activeEnemies++;
            }

        }
        else
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].SetActive(true);
                enemies.RemoveAt(i);
                activeEnemies++;
            }
    }

    public void EnemyDied(GameObject _deadEnemy)
    {
        activeEnemies--;
        enemies.Add(_deadEnemy);
        _deadEnemy.transform.position = transform.position;
        _deadEnemy.SetActive(false);

    }

}
