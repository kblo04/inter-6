﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    // Start is called before the first frame update

    public Animator animator;

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.name == "CheckGatePass" && other.gameObject.tag == "Player")
        {
            animator.SetTrigger("openDoor");
        }
        else if (gameObject.name == "CheckDoorPass" && other.gameObject.tag == "Player")
        {
            animator.SetTrigger("closeDoor");
        }

    }
}
