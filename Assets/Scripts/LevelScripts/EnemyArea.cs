﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyArea : MonoBehaviour
{
    public List<StateController> enemies = new List<StateController>();
    public delegate void OnCombatActivation();
    public event OnCombatActivation onCombatActivation;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (enemies.Count > 0)
            {
                for (int i = 0; i < enemies.Count; i++)
                {
                    enemies[i].StartCombat();
                    enemies[i].target = other.transform;
                }
            }
            onCombatActivation?.Invoke();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
            CloseArea();

    }


    private void CheckEnemyDeath()
    {
        if (enemies.Count > 0)
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i].currentState == !enemies[i].gameObject.activeInHierarchy)
                {
                    enemies.RemoveAt(i);
                }
            }
        }
    }

    private void CloseArea()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }
    }

    private void Update()
    {
        CheckEnemyDeath();
        if (enemies.Count == 0 && !GetComponentInParent<EnemySpawner>())
        {
            OpenArea();
        }
    }

    private void OpenArea()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
            transform.GetChild(i).gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }

}
