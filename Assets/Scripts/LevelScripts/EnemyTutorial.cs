﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTutorial : MonoBehaviour
{
    // Start is called before the first frame update
    public TutorialManager tutorialManager;
    private HitPoints hitPoints;
    private bool sawPanel;

    void Start()
    {
        hitPoints = GetComponent<HitPoints>();
    }

    // Update is called once per frame
    void Update()
    {
        if(hitPoints.currentPoints <= hitPoints.basePoints/2 && !sawPanel)
        {
            tutorialManager.ActivePanel(1);
            sawPanel = true;
        }
    }
}
