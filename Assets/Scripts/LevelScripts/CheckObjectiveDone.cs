﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckObjectiveDone : MonoBehaviour
{
    // Start is called before the first frame update
    [HideInInspector] public bool objectiveIsDone;
    private HitPoints hitPoints;
    public ExplosionEffects explosionEffects;
    public GameObject explosionVFX;
    public GameObject light;
    private AudioSource audioSource;
    private bool exploded;

    private void Start()
    {
        hitPoints = GetComponent<HitPoints>();
        audioSource = GetComponent<AudioSource>();
        explosionVFX.SetActive(false);
        hitPoints.onDamage += PlayHitEffects;
    }

    private void OnDisable()
    {
        hitPoints.onDamage -= PlayHitEffects;

    }

    private void Update()
    {
        if (hitPoints.currentPoints == 0)
            objectiveIsDone = true;

        if (objectiveIsDone)
        {
            Unparent();
            if (!exploded)
                PlayExplosionEffects();
        }

        if (exploded)
        {
            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<BoxCollider>().enabled = false;
            light.SetActive(false);
        }
    }

    private void Unparent()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<StateController>())
            {
                AddToList(transform.GetChild(i).GetComponent<StateController>());
            }
        }
        transform.DetachChildren();

    }

    private void AddToList(StateController _enemyAlive)
    {
        GetComponentInChildren<EnemyArea>().enemies.Add(_enemyAlive);
    }

    private void PlayExplosionEffects()
    {
        explosionVFX.transform.position = transform.position;
        explosionVFX.transform.rotation = transform.rotation;
        explosionVFX.SetActive(true);

        audioSource.clip = (explosionEffects.explosionSFXs.audioClip[Random.Range(0, explosionEffects.explosionSFXs.audioClip.Length)]);
        audioSource.volume = explosionEffects.explosionSFXs.volume;
        audioSource.Play();

        exploded = true;
    }

    private void PlayHitEffects()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.clip = (explosionEffects.hitSFXs.audioClip[Random.Range(0, explosionEffects.hitSFXs.audioClip.Length)]);
            audioSource.volume = explosionEffects.hitSFXs.volume;
            audioSource.Play();
        }

    }
}
