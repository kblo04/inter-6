﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckLevelDone : MonoBehaviour
{
    // Start is called before the first frame update
    public List<GameObject> levelObjectives = new List<GameObject>();
    public List<GameObject> fires = new List<GameObject>();
    public List<GameObject> enemiesAreaNearby = new List<GameObject>();
    public Material objectiveCompleteMat;
    public bool cameIn;
    private bool animationViewed;
    private bool enemiesNearby;




    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < levelObjectives.Count; i++)
        {
            if(levelObjectives[i].GetComponent<HitPoints>().currentPoints <= 0)
            {
                levelObjectives.RemoveAt(i);
            }
        }

        CheckObjectivesAlive();
        CheckLists();

        if (levelObjectives.Count == 0 && cameIn && !animationViewed && !enemiesNearby)
        {
            GetComponentInChildren<Animation>().Play();
            animationViewed = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && levelObjectives.Count == 0)
            cameIn = true;
    }

    void CheckObjectivesAlive()
    {
        if (levelObjectives.Count == 0 && !enemiesNearby)
        {
            for (int i = 0; i < fires.Count; i++)
            {
                fires[i].SetActive(true);
            }
        }
    }

    void CheckLists()
    {
        for (int i = 0; i < enemiesAreaNearby.Count; i++)
        {
            if (enemiesAreaNearby[i].GetComponent<EnemyArea>().enemies.Count == 0)
            {
                enemiesNearby = false;
            }
            else
                enemiesNearby = true;
        }
    }
}
