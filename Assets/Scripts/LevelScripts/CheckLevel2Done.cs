﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckLevel2Done : MonoBehaviour
{
    // Start is called before the first frame update
    public List<GameObject> levelObjectives = new List<GameObject>();
    public List<GameObject> objectivesToActiveWithPapa = new List<GameObject>();
    public StateController Papa;
    public SceneLoader gameState;
    private bool activedPapa;
    private bool camein;

    private void Start()
    {
        Papa.gameObject.SetActive(false);
    }

    void Update()
    {
        for (int i = 0; i < levelObjectives.Count; i++)
        {
            if (levelObjectives[i].GetComponent<HitPoints>() && levelObjectives[i].GetComponent<HitPoints>().currentPoints == 0)
            {
                levelObjectives.RemoveAt(i);
            }
        }

        if (levelObjectives.Count == 0 && !activedPapa && camein)
        {
            ActivePapa();
            activedPapa = true;
        }

        if(activedPapa && Papa.GetComponent<HitPoints>().currentPoints == 0 && !Papa.gameObject.activeInHierarchy)
        {
            EndLevel();
        }

    }

    public void ActivePapa()
    {
        for (int i = 0; i < objectivesToActiveWithPapa.Count; i++)
        {
            if (!objectivesToActiveWithPapa[i].activeInHierarchy)
                objectivesToActiveWithPapa[i].SetActive(true);

        }
        Papa.gameObject.SetActive(true);
        Papa.StartCombat();
    }

    public void EndLevel()
    {
        gameState.LoadLevel(0);

    }

    private void OnTriggerEnter(Collider other)
    {
        if(levelObjectives.Count == 0 && !activedPapa && other.gameObject.tag == "Player")
        {
            camein = true;
        }
    }
}
