﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    public GameObject droppedItem;
    public GameObject input;
    public TroopDeath troopDeath;
    public bool hasInput;

    private void Awake()
    {
        troopDeath = GetComponent<TroopDeath>();
        troopDeath.onDeath += Drop;
    }

    public void Drop()
    {
        droppedItem.SetActive(true);
        droppedItem.transform.position = transform.position;
        if (hasInput)
        {
            input.SetActive(true);
            input.transform.position = transform.position + transform.up * 2f;
        }
        transform.parent = null;

    }
}
