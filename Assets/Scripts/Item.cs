﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public KeyCode input = KeyCode.E;
    private bool textOn = false;

    public float baseTimer = 1;
    private float timer = 0;

    [TextArea(3, 10)] public string text;

    private void OnTriggerStay(Collider other)
    {
        
        if(Input.GetKeyDown(input) && other.CompareTag("Player"))
        {
            if(!textOn && timer >= baseTimer)
            {
                TextPanel.sharedInstance.TextOn(text);
                textOn = true;
                timer = 0;
            }
        }
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetKeyDown(input))
        {
            if (textOn && timer >= baseTimer)
            {
                TextPanel.sharedInstance.TextOff();
                textOn = false;
                timer = 0;
            }
        }
    }
}
