﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBulletLauncher : MonoBehaviour
{
    private Rigidbody rb;
    [HideInInspector]public float force = 1;
    [SerializeField] float baseLifeTime = 5f;
    private float lifeTime;



    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        lifeTime = baseLifeTime;
    }


    private void OnEnable()
    {
        rb.isKinematic = false;
        rb.velocity = transform.forward * force;
        lifeTime = baseLifeTime;
    }

    private void OnDisable()
    {
        rb.isKinematic = true;
    }

    private void Update()
    {
        if(gameObject.activeInHierarchy)
        {
            GoFoward();
        }
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            SelfDestruct();
        }
    }

    void GoFoward()
    {
        rb.velocity = transform.forward * force;
    }

    private void SelfDestruct()
    {
        ObjectPooler.SharedInstance.PoolDestroy(gameObject);
    }
}
