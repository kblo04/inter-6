﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySfx : MonoBehaviour
{
    // Start is called before the first frame update
    public SoundBank soundBank;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayAntecipationSfx()
    {

        audioSource.clip = (soundBank.antecipation.audioClip[Random.Range(0, soundBank.antecipation.audioClip.Length)]);
        audioSource.volume = soundBank.antecipation.volume;
        audioSource.Play();

    }

    public void PlayImpactSfx()
    {

        audioSource.clip = (soundBank.impact.audioClip[Random.Range(0, soundBank.impact.audioClip.Length)]);
        audioSource.volume = soundBank.antecipation.volume;
        audioSource.Play();

    }


}
