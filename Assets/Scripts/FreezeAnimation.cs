﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeAnimation : MonoBehaviour
{
    [HideInInspector]public Animator currentAnimator;

    public void Freeze(Animator animator, float freezeDuration)
    {
        currentAnimator = animator;
        currentAnimator.speed = 0;
        StartCoroutine(Unfreeze(currentAnimator, freezeDuration));
    }

    IEnumerator Unfreeze(Animator animator, float freezeDuration)
    {
        yield return new WaitForSeconds(freezeDuration);
        animator.speed = 1;
        yield return null;
    }

    public void ForceUnfreeze()
    {
        if(currentAnimator)
        {
            currentAnimator.speed = 1;
        }
    }
}
