﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasState : MonoBehaviour
{
    public Animator[] buttons;

    public void CanvasOn()
    {
        for(int i = 0; i < buttons.Length; i++)
        {
            buttons[i].enabled = true;
        }
    }

}
