﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HitPoints : Points
{
    [HideInInspector] public bool invulnerable = false;
    private bool runLastTimeHittedTimer;
    public float lastTimeHitted;
    public float timeBetweenHitAllow = 0.2f;

    public override event OnDamage onDamage;

    public override void Start()
    {
        currentPoints = basePoints;
        lastTimeHitted = timeBetweenHitAllow + 1f;
        if (slider != null)
        {
            slider.maxValue = basePoints;
        }
    }

    public override void Update()
    {
        UpdateSlider();
        LimitPoints();

        if (runLastTimeHittedTimer)
        {
            lastTimeHitted += Time.deltaTime;
        }
    }

    public override void Damage(int damageToDeal)
    {
        if (!invulnerable && lastTimeHitted > timeBetweenHitAllow)
        {
            StartTimer();
            currentPoints -= damageToDeal;
            onDamage?.Invoke();

        }
    }

    public void StartTimer()
    {
        runLastTimeHittedTimer = true;
        lastTimeHitted = 0;
    }

}
