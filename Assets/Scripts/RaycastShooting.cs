﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastShooting : MonoBehaviour
{
    public ObjectPooler bulletPooler;
    public Transform shootPos;
    public float trailLifeTime = .1f;
    private LineRenderer lineRenderer;
    [HideInInspector] public List<GameObject> activeBullets;

    private void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }


    public GameObject ShootBullet(float range, float radius)
    {
        RaycastHit hit;
        Physics.SphereCast(shootPos.position,radius ,Camera.main.transform.forward, out hit, range);
        if (hit.point != new Vector3(0,0,0))
        {
            if(activeBullets.Count < bulletPooler.amountToPool)
            {
                SetBullet(hit);
            }
            else if (activeBullets.Count >= bulletPooler.amountToPool)
            {
                RespawnActiveBullet(hit);
            }
            DrawTrail(shootPos.position, hit.point);
            return hit.transform.gameObject;

        }
        return null;

    }

    GameObject SetBullet(RaycastHit bulletPos)
    {

        GameObject bullet = bulletPooler.PoolInstantiate(bulletPos.point, Camera.main.transform.rotation);
        activeBullets.Add(bullet);
        bullet.transform.parent = bulletPos.transform;
        return bullet;

    }

    private void RespawnActiveBullet(RaycastHit hit)
    {
        DestroyFirstActiveBullet();
        SetBullet(hit);
    }

    public void DestroyFirstActiveBullet()
    {
        bulletPooler.PoolDestroy(activeBullets[0]);
        activeBullets.RemoveAt(0);
    }

    public void DrawTrail(Vector3 _initialPos, Vector3 _finalPos)
    {
        lineRenderer.SetPosition(0, _initialPos);
        lineRenderer.SetPosition(1, _finalPos);
        StartCoroutine("ClearTrail");
    }


    IEnumerator ClearTrail()
    {
        yield return new WaitForSeconds(trailLifeTime);
        lineRenderer.SetPosition(0, Vector3.zero);
        lineRenderer.SetPosition(1, Vector3.zero);
    }

}
