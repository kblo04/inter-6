﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSpawners : MonoBehaviour
{
    public Transform[] spawnersPosition;
    public GameObject spawner;
    public Transform playerTransform;
    public GameObject explosionVfx;
    // Start is called before the first frame update    

    public void SpawnersOn()
    {

        for (int i = 0; i < spawnersPosition.Length; i++)
        {
            if (spawnersPosition[i].childCount == 0 || !spawnersPosition[i].GetChild(0).GetComponent<BoxCollider>().enabled && spawnersPosition[i].childCount == 1)
            {
                GameObject spawnerInst = Instantiate(spawner, spawnersPosition[i]);
                spawnerInst.GetComponent<EnemySpawner>().player = playerTransform;
                spawnerInst.GetComponent<EnemySpawner>().enemiesPerWave = 1;
                spawnerInst.GetComponent<EnemySpawner>().enemiesTotalNum = 1;
                spawnerInst.GetComponent<HitPoints>().basePoints = 20;
                spawnerInst.GetComponent<CheckObjectiveDone>().explosionVFX = explosionVfx;
            }

        }
    }
}
